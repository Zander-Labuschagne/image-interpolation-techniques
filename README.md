# Image Interpolation Techniques

Interpolation techniques used for image resolution enhancement. During my honours year I had to write a 'mock' IEEE article for my image processing subject, this article included downscaling and upscaling of images, which ended up focussing more on the different interpolation methods used for upscaling. I implemented various downscaling methods and upscaling methods which included comparisons of many bicubic interpolation methods alongside the more trivial bilinear and nearest neighbour methods.

* Includes MATLAB and C++ implementations.

## C++ Implementation
Currently these are implemented for a specific use case, interpolating plain values read from a KML file, at the moment longitude and latitude. Since this doesn't require cross dimensional interpolation I don't consider these to be bicubic/bilinear interpolation, but instead plain old cubic/linear interpolation on multiple data sets. This also allowed me to interpolate both dimensions simultaneously instead of one after the other.

#### Requirements
* **pugixml** to parse KML. It is a Light-weight, simple and fast XML parser for C++ with XPath support.

#### Other Notes
* I have used `gnuplot` to plot the results. The results are written to `txt` files from C++, which are in turn read by `gnuplot` to plot them.

## TODO's
* Implement image interpolation in C++. Will very likely use OpenCV and compare to OpenCV's methods.
* Attempt to read altitudes and velocity from KML and interpolate those as well.
* MATLAB scripts needs improvements and documentation.