#include <iostream>
#include <fstream>
#include <math.h>
#include <algorithm>
#include <vector>

#include <pugixml.hpp>

/**
 * Represents a point entry in the KML file.
 */
struct Point {
	float lon;
	float lat;
};

/**
 * Loads points from the given KML file. 
 * This currently alters the data read from the file as I don't have more interesting paths, the alteration gives me a more interesting scenario.
 */
std::vector<Point> loadTargetTrack(const std::string &strFilePath)
{
	pugi::xml_document doc;
	if(!doc.load_file(strFilePath.c_str())) {
		std::cerr << "Error: Could not load track file " << strFilePath << std::endl;
		throw;
	}

    	std::vector<Point> points;
	pugi::xml_node Folder = doc.child("kml").child("Document").child("Folder");
	std::string strTimeStampField = "Timestamp_us=";
	float deviator = 0;
	float deviator_driver = 0.01;
	float deviator_controller = 0.01;
	int i = 0;

	for(pugi::xml_node Placemark = Folder.child("Placemark"); Placemark; Placemark = Placemark.next_sibling("Placemark")) {
		std::string strDescription = Placemark.child_value("description");
		std::string strCoordinates = Placemark.child("Point").child_value("coordinates");
		++i;

		if(strDescription != "" && strCoordinates != "") {
			std::string::size_type Found = strDescription.find(strTimeStampField);

			if (Found != std::string::npos) {
				std::string strTimeStamp = strDescription.substr(Found + strTimeStampField.size(), 16); // Microsecond timestamp should have 16 digits.
				std::string::size_type sz;
				double dLon = std::stod(strCoordinates, &sz);
				double dLat = std::stod(strCoordinates.substr(sz + 1));

				Point point;
				point.lon = dLon + deviator; // Altering the longitude.
				point.lat = dLat;

				// Add less boring behaviour to the alteration.
				deviator += deviator_driver;
				deviator_driver += deviator_controller;
				if (deviator_driver > 1) {
					deviator_controller = -0.025;
				}
				else if (deviator_driver < -1) {
					deviator_controller = 0.00001;
				}
				// std::cout << deviator << std::endl;
				if(i % 20 == 0 && i < 200) {
					
					points.push_back(point);
				}
			}
		}
	}
	points.erase(points.begin() + 5);
	points.erase(points.begin() + 6);

	return points;
}

/**
 * Writes to a file which can be used by gnuplot to plot them.
 * */
void display(Point *values, int length, std::string file_name) {
	std::ofstream outfile(file_name);
	outfile.precision(6);
	// for(int i = 20; i < length - 20; ++i) { // Weird stuff around the edges.
	for(int i = 0; i < length; ++i) {
        	outfile << std::fixed << values[i].lat << "   " << std::fixed << values[i].lon << std::endl;
    	}
    	outfile.close();
}

/**
 * Returns a calculated lambda, commonly used in all cubic and linear interpolation methods.
 * */
float get_lambda(int i, float interpolation_ratio) {
	return (i - 1) * interpolation_ratio - floor((i - 1) * interpolation_ratio);
}

/**
 * Calculates indices to read true values from, commonly used in all cubic interpolation methods.
 * */
void get_read_indices(int i, float interpolation_ratio, int cap, int &x0, int &x1, int &x2, int &x3)
{
	// min and max ensures that we don't try to read a negative index or an index that exceeds the bounds of the vector.
        x0 = std::min(cap - 1.0f, std::max(0.0f, floor((i - 1) * interpolation_ratio) - 1));
        x1 = std::min(cap - 1.0f, std::max(0.0f, floor((i - 1) * interpolation_ratio)));
        x2 = std::min(cap - 1.0f, std::max(0.0f, ceil((i - 1) * interpolation_ratio)));
        x3 = std::min(cap - 1.0f, std::max(0.0f, ceil((i - 1) * interpolation_ratio) + 1));
}

/**
 * Returns a calculated R3 value, Used in McAndrew's cubic interpolation method.
 * */
float r3(float u) {
	if (abs(u) <= 1) {
        	return 1.5 * pow(abs(u), 3) - 2.5 * pow(abs(u), 2) + 1;
        }
    	else if(abs(u) > 1 && abs(u) <= 2) {
        	return -0.5 * pow(abs(u), 3) + 2.5 * pow(abs(u), 2) - 4 * abs(u) + 2;
    	}
}

/**
 * Calculates variables that are used in the Catmull-Rom cubic interpolation method.
 * */
void calculate_catmull_rom_variables(float y0, float y1, float y2, float y3, float &a0, float &a1, float &a2, float &a3) {
		a0 = -0.5 * y0 + 1.5 * y1 - 1.5 * y2 + 0.5 * y3;
            	a1 = y0 - 2.5 * y1 + 2 * y2 - 0.5 * y3;
            	a2 = -0.5 * y0 + 0.5 * y2;
            	a3 = y1;
}

/**
 * Calculates variables that are used in the Hermite cubic interpolation method.
 * */
void calculate_hermite_variables(float y0, float y1, float y2, float y3, float lambda, float tension, float bias, float &m0, float &m1, float &a0, float &a1, float &a2, float &a3) {
	m0 = ((y1 - y0) * (1 + bias) * (1 - tension) / 2.0) + ((y2 - 1) * (1 - bias) * (1 - tension) / 2.0);
        m1 = ((y2 - y1) * (1 + bias) * (1 - tension) / 2.0) + ((y3 - y2) * (1 - bias) * (1 - tension) / 2.0);
            
        a0 = 2 * pow(lambda, 3) - 3 * pow(lambda, 2) + 1;
        a1 = pow(lambda, 3) - 2 * pow(lambda, 2) + lambda;
        a2 = pow(lambda, 3) - pow(lambda, 2);
        a3 = -2 * pow(lambda, 3) + 3 * pow(lambda, 2);
}

/**
 * Cubic interpolation function common to all Mitchell-Netravali type interpolation methods.
 * */
float cubic_interpolation_function(float b, float c, float y0, float y1, float y2, float y3, float lambda)
{
	return ((-1.0 / 6.0 * b - c) * y0 + (-3.0 / 2.0 * b - c + 2) * y1 + (3.0 / 2.0 * b + c - 2) * y2 + (1.0 / 6.0 * b + c) * y3) * pow(lambda, 3)
            	+ ((1.0 / 2.0 * b + 2 * c) * y0 + (2.0 * b + c - 3) * y1 + (-5.0 / 2.0 * b - 2 * c + 3) * y2 - c * y3) * pow(lambda, 2)
            	+ ((-1.0 / 2.0 * b - c) * y0 + (1.0 / 2.0 * b + c) * y2) * lambda + 1.0 / 6.0 * b * y0 + (-1.0 / 3.0 * b + 1) * y1 + 1.0 / 6.0 * b * y2; 
}

/**
 * Cubic interpolation function used in the Catmull-Rom cubic interpolation method.
 * */
float catmull_rom_cubic_interpolation_function(float a0, float a1, float a2, float a3, float lambda) {
	return a0 * pow(lambda, 3) + a1 * pow(lambda, 2) + a2 * lambda + a3;
}

/**
 * Cubic interpolation function used in the  Hermite cubic interpolation method.
 * */
float hermite_cubic_interpolation_function(float y1, float y2, float a0, float a1, float a2, float a3, float m0, float m1) {
	return a0 * y1 + a1 * m0 + a2 * m1 + a3 * y2;
}

/**
 * Cubic interpolation function used in the  LaGrange cubic interpolation method.
 * FIXME
 * */
float lagrange_cubic_interpolation_function(float lambda, float x1, float x2, float x3, float y1, float y2, float y3) {
	return (((lambda - x2) * (lambda - x3)) / ((x1 - x2) * (x1 - x3))) * y1 + (((lambda - x1) * (lambda - x3)) / ((x2 - x1) * (x2 - x3))) * y2 + (((lambda - x1) * (lambda - x2)) / ((x3 - x1) * (x3 - x2))) * y3;
}

/**
 * Cubic interpolation function used in the cubic interpolation method by McAndrew.
 * */
float mcandrew_cubic_interpolation_function(float lambda, float y0, float y1, float y2, float y3) {
	return r3(-1 - lambda) * y0 + r3(-lambda) * y1 + r3(1 - lambda) * y2 + r3(2 - lambda) * y3;
}

//FIXME
// float pixInsight_cubic_interpolation_function(float lambda, float y0, float y1, float y2, float y3) {
// 	return r3_pixInsight(-1 - lambda) * y0 + r3_pixInsight(-lambda) * y1 + r3_pixInsight(1 - lambda) * y2 + r3_pixInsight(2 - lambda) * y3;
// }

/**
 * Linear interpolation function.
 * */
float linear_interpolation_function(float lambda, float y1, float y2) {
	return lambda * y2 + (1 - lambda) * y1;
}

/**
 * Algorithm that performs Catmull-Rom cubic interpolation.
 */
Point *catmull_rom_cubic_interpolation(Point *original_values, int original_size, int desired_size) {
	float interpolation_ratio = (float)original_size / (float)desired_size; // I don't know what to name this variable.

    	Point *interpolated_values = new Point[desired_size];
    	interpolated_values[0].lat = original_values[0].lat;
    	interpolated_values[0].lon = original_values[0].lon;

    	for (int i = 1; i < desired_size; ++i) {
        	float lambda = get_lambda(i, interpolation_ratio);
       		int x0, x1, x2, x3;
        	get_read_indices(i, interpolation_ratio, original_size, x0, x1, x2, x3);

        	float y3 = original_values[x3].lat;
        	float y2 = original_values[x2].lat;
        	float y0 = original_values[x0].lat;
        	float y1 = original_values[x1].lat;

        	float a0, a1, a2, a3;
        	calculate_catmull_rom_variables(y0, y1, y2, y3, a0, a1, a2, a3);
            
            	interpolated_values[i].lat = catmull_rom_cubic_interpolation_function(a0, a1, a2, a3, lambda);
            	// interpolated_values[i].lat = y1 + 0.5 * lambda * (y2 - y0 + lambda * (2 * y0 - 5 * y1 + 4 * y2 - y3 + lambda * (3 * (y1 - y2) + y3 - y0)));

        	y3 = original_values[x3].lon;
        	y2 = original_values[x2].lon;
        	y0 = original_values[x0].lon;
        	y1 = original_values[x1].lon;

        	calculate_catmull_rom_variables(y0, y1, y2, y3, a0, a1, a2, a3);

        	interpolated_values[i].lon = catmull_rom_cubic_interpolation_function(a0, a1, a2, a3, lambda);
            	// interpolated_values[i].lat = y1 + 0.5 * lambda * (y2 - y0 + lambda * (2 * y0 - 5 * y1 + 4 * y2 - y3 + lambda * (3 * (y1 - y2) + y3 - y0)));
    	}

    	return interpolated_values;
}

/**
 * Algorithm that performs Hermite cubic interpolation.
 */
Point *hermite_cubic_interpolation(Point *original_values, int original_size, int desired_size, float tension, float bias) {
	float interpolation_ratio = (float)original_size / (float)desired_size; // I don't know what to name this variable.
    	Point *interpolated_values = new Point[desired_size];
    	//float tension = 1; //1 is high, 0 is normal, -1 is low
   	//float bias = 1;//%0 is even, positive is towards first segment, negative is towards the other

    	interpolated_values[0].lat = original_values[0].lat;
    	interpolated_values[0].lon = original_values[0].lon;

    	for (int i = 1; i < desired_size; ++i) {
        	float lambda = get_lambda(i, interpolation_ratio);
       		int x0, x1, x2, x3;
        	get_read_indices(i, interpolation_ratio, original_size, x0, x1, x2, x3);

        	float y3 = original_values[x3].lat;
        	float y2 = original_values[x2].lat;
        	float y0 = original_values[x0].lat;
        	float y1 = original_values[x1].lat;

        	float a0, a1, a2, a3, m0, m1;
        	calculate_hermite_variables(y0, y1, y2, y3, lambda, tension, bias, m0, m1, a0, a1, a2, a3);

            	interpolated_values[i].lat = hermite_cubic_interpolation_function(y1, y2, a0, a1, a2, a3, m0, m1);

        	y3 = original_values[x3].lon;
        	y2 = original_values[x2].lon;
        	y0 = original_values[x0].lon;
        	y1 = original_values[x1].lon;

        	calculate_hermite_variables(y0, y1, y2, y3, lambda, tension, bias, m0, m1, a0, a1, a2, a3);

            	interpolated_values[i].lon = hermite_cubic_interpolation_function(y1, y2, a0, a1, a2, a3, m0, m1);
    	}

    	return interpolated_values;

}

/**
 * Algorithm that performs LaGrange cubic interpolation.
 * FIXME
 */
Point *lagrange_cubic_interpolation(Point *original_values, int original_size, int desired_size) {
	float interpolation_ratio = (float)original_size / (float)desired_size; // I don't know what to name this variable.
    	Point *interpolated_values = new Point[desired_size];

    	interpolated_values[0].lat = original_values[0].lat;
    	interpolated_values[0].lon = original_values[0].lon;

    	for (int i = 1; i < desired_size; ++i) {
        	float lambda = get_lambda(i, interpolation_ratio);
       		int x0, x1, x2, x3;
        	get_read_indices(i, interpolation_ratio, original_size, x0, x1, x2, x3);

        	float y3 = original_values[x3].lat;
        	float y2 = original_values[x2].lat;
        	float y0 = original_values[x0].lat;
        	float y1 = original_values[x1].lat;

            	interpolated_values[i].lat = lagrange_cubic_interpolation_function(lambda, x1, x2, x3, y1, y2, y3);

        	y3 = original_values[x3].lon;
        	y2 = original_values[x2].lon;
        	y0 = original_values[x0].lon;
        	y1 = original_values[x1].lon;

            	interpolated_values[i].lon = lagrange_cubic_interpolation_function(lambda, x1, x2, x3, y1, y2, y3);
    	}

    	return interpolated_values;
}

/**
 * Algorithm that performs cubic interpolation as explained by McAndrew in his Introduction to Digital Image Processing textbook.
 */
Point *mcandrew_cubic_interpolation(Point *original_values, int original_size, int desired_size) {
	float interpolation_ratio = (float)original_size / (float)desired_size; // I don't know what to name this variable.
    	Point *interpolated_values = new Point[desired_size];

    	interpolated_values[0].lat = original_values[0].lat;
    	interpolated_values[0].lon = original_values[0].lon;

    	for (int i = 1; i < desired_size; ++i) {
        	float lambda = get_lambda(i, interpolation_ratio);
       		int x0, x1, x2, x3;
        	get_read_indices(i, interpolation_ratio, original_size, x0, x1, x2, x3);

        	float y3 = original_values[x3].lat;
        	float y2 = original_values[x2].lat;
        	float y0 = original_values[x0].lat;
        	float y1 = original_values[x1].lat;

            	interpolated_values[i].lat = mcandrew_cubic_interpolation_function(lambda, y0, y1, y2, y3);

        	y3 = original_values[x3].lon;
        	y2 = original_values[x2].lon;
        	y0 = original_values[x0].lon;
        	y1 = original_values[x1].lon;

            	interpolated_values[i].lon = mcandrew_cubic_interpolation_function(lambda, y0, y1, y2, y3);
    	}

    	return interpolated_values;
}

// FIXME
// Point *pixInsight_cubic_interpolation(Point *original_values, int original_size, int desired_size) {
// 	float interpolation_ratio = (float)original_size / (float)desired_size; // I don't know what to name this variable.
//     	Point *interpolated_values = new Point[desired_size];

//     	interpolated_values[0].lat = original_values[0].lat;
//     	interpolated_values[0].lon = original_values[0].lon;

//     	for (int i = 1; i < desired_size; ++i) {
//         	float lambda = get_lambda(i, interpolation_ratio);
//        		int x0, x1, x2, x3;
//         	get_read_indices(i, interpolation_ratio, x0, x1, x2, x3);

//         	float y3 = original_values[x3].lat;
//         	float y2 = original_values[x2].lat;
//         	float y0 = original_values[x0].lat;
//         	float y1 = original_values[x1].lat;

//             	interpolated_values[i].lat = pixInsight_cubic_interpolation_function(lambda, y0, y1, y2, y3);

//         	y3 = original_values[x3].lon;
//         	y2 = original_values[x2].lon;
//         	y0 = original_values[x0].lon;
//         	y1 = original_values[x1].lon;

//             	interpolated_values[i].lon = pixInsight_cubic_interpolation_function(lambda, y0, y1, y2, y3);
//     	}

//     	return interpolated_values;
// }

/**
 * Algorithm that performs the Mitchell-Netravali type cubic interpolation.
 * Examples include B-Splines, C-Splines and the Standard Mitchell-Netravali reconstruction filter.
 * This can also be used to perform Catmull-Rom cubic interpolation.
 */
Point *cubic_interpolation(Point *original_values, int original_size, int desired_size, float b, float c) {
    	float interpolation_ratio = (float)original_size / (float)desired_size; // I don't know what to name this variable.

    	Point *interpolated_values = new Point[desired_size];
    	interpolated_values[0].lat = original_values[0].lat;
    	interpolated_values[0].lon = original_values[0].lon;

    	for (int i = 1; i < desired_size; ++i) {
        	float lambda = get_lambda(i, interpolation_ratio);
       		int x0, x1, x2, x3;
        	get_read_indices(i, interpolation_ratio, original_size, x0, x1, x2, x3);

        	float y3 = original_values[x3].lat;
        	float y2 = original_values[x2].lat;
        	float y0 = original_values[x0].lat;
        	float y1 = original_values[x1].lat;
        	interpolated_values[i].lat = cubic_interpolation_function(b, c, y0, y1, y2, y3, lambda);

        	y3 = original_values[x3].lon;
        	y2 = original_values[x2].lon;
        	y0 = original_values[x0].lon;
        	y1 = original_values[x1].lon;
        	interpolated_values[i].lon = cubic_interpolation_function(b, c, y0, y1, y2, y3, lambda);
    	}

    	return interpolated_values;
}

/**
 * Algorithm that performs linear interpolation.
 */
Point *linear_interpolation(Point *original_values, int original_size, int desired_size) {
    	float interpolation_ratio = (float)original_size / (float)desired_size; // I don't know what to name this variable.

    	Point *interpolated_values = new Point[desired_size];
    	interpolated_values[0].lat = original_values[0].lat;
    	interpolated_values[0].lon = original_values[0].lon;

    	for (int i = 1; i < desired_size; ++i) {
        	float lambda = get_lambda(i, interpolation_ratio);
        	int x2 = ceil((i - 1) * interpolation_ratio);
        	int x1 = floor((i - 1) * interpolation_ratio);

        	float y2 = original_values[x2].lat;
            	float y1 = original_values[x1].lat;

        	interpolated_values[i].lat = linear_interpolation_function(lambda, y1, y2);

		y2 = original_values[x2].lon;
            	y1 = original_values[x1].lon;
        	interpolated_values[i].lon = linear_interpolation_function(lambda, y1, y2);
    	}

    	return interpolated_values;
}

int main()
{
    	std::vector<Point> points = loadTargetTrack("Tracks_2021-05-12T14_14_49Z.kml");
    	display(&points[0], points.size(), "originals.txt");

    	// Interpolation
    	float desired_size = 10000;
    	std::cout << "Points loaded: " << points.size() << std::endl;

 	// Standard Mitchell-Netravali (used in ImageMagick)
    	Point *mn_interpolated_points = cubic_interpolation(&points[0], points.size(), desired_size, 1.0 / 3.0, 1.0 / 3.0);
    	display(mn_interpolated_points, desired_size, "Mitchell-Netravali.txt");

 	// C-Spline
    	Point *c_interpolated_points = cubic_interpolation(&points[0], points.size(), desired_size, 0.0, 1.0);
    	display(c_interpolated_points, desired_size, "C-Spline.txt");

 	// B-Spline
    	Point *b_interpolated_points = cubic_interpolation(&points[0], points.size(), desired_size, 1.0, 0.0);
    	display(b_interpolated_points, desired_size, "B-Spline.txt");

    	// Photoshop's C-Spline
    	Point *ps_interpolated_points = cubic_interpolation(&points[0], points.size(), desired_size, 0.0, 0.75);
    	display(ps_interpolated_points, desired_size, "Photoshop Method.txt");

 	// Catmull-Rom (reusing standard Mitchell-Netravali) (used in GIMP)
    	Point *cra_interpolated_points = cubic_interpolation(&points[0], points.size(), desired_size, 0.0, 0.5);
    	display(cra_interpolated_points, desired_size, "Catmull-Rom(Alternative).txt");

    	// Catmull-Rom proper
    	Point *crp_interpolated_points = catmull_rom_cubic_interpolation(&points[0], points.size(), desired_size);
    	display(crp_interpolated_points, desired_size, "Catmull-Rom.txt");

    	// Hermite (High tension, negative bias)
    	Point *h_interpolated_points = hermite_cubic_interpolation(&points[0], points.size(), desired_size, 1, 1);
    	display(h_interpolated_points, desired_size, "Hermite(High tension, negative bias).txt");
    	// Hermite (High tension, unbiased)
    	h_interpolated_points = hermite_cubic_interpolation(&points[0], points.size(), desired_size, 1, 0);
    	display(h_interpolated_points, desired_size, "Hermite(High tension, unbiased).txt");
    	// Hermite (High tension, positive bias)
    	h_interpolated_points = hermite_cubic_interpolation(&points[0], points.size(), desired_size, 1, -1);
    	display(h_interpolated_points, desired_size, "Hermite(High tension, positive bias).txt");
    	// Hermite (Normal tension, negative bias)
    	h_interpolated_points = hermite_cubic_interpolation(&points[0], points.size(), desired_size, 0, 1);
    	display(h_interpolated_points, desired_size, "Hermite(Normal tension, negative bias).txt");
    	// Hermite (Normal tension, unbiased)
    	h_interpolated_points = hermite_cubic_interpolation(&points[0], points.size(), desired_size, 0, 0);
    	display(h_interpolated_points, desired_size, "Hermite(Normal tension, unbiased).txt");
    	// Hermite (Normal tension, positive bias)
    	h_interpolated_points = hermite_cubic_interpolation(&points[0], points.size(), desired_size, 0, -1);
    	display(h_interpolated_points, desired_size, "Hermite(Normal tension, positive bias).txt");
    	// Hermite (Low tension, negative bias)
    	h_interpolated_points = hermite_cubic_interpolation(&points[0], points.size(), desired_size, -1, 1);
    	display(h_interpolated_points, desired_size, "Hermite(Low tension, negative bias).txt");
    	// Hermite (Low tension, unbiased)
    	h_interpolated_points = hermite_cubic_interpolation(&points[0], points.size(), desired_size, -1, 0);
    	display(h_interpolated_points, desired_size, "Hermite(Low tension, unbiased).txt");
    	// Hermite (Low tension, positive bias)
    	h_interpolated_points = hermite_cubic_interpolation(&points[0], points.size(), desired_size, -1, -1);
    	display(h_interpolated_points, desired_size, "Hermite(Low tension, positive bias).txt");

    	// LaGrange
    	Point *lg_interpolated_points = lagrange_cubic_interpolation(&points[0], points.size(), desired_size);
    	display(lg_interpolated_points, desired_size, "LaGrange.txt");

    	// McAndrew
    	Point *mca_interpolated_points = mcandrew_cubic_interpolation(&points[0], points.size(), desired_size);
    	display(mca_interpolated_points, desired_size, "McAndrew.txt");

    	// Linear
    	Point *lin_interpolated_points = linear_interpolation(&points[0], points.size(), desired_size);
    	display(lin_interpolated_points, desired_size, "Linear.txt");

	return 0;
}