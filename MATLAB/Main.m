close all;
clear all;
clc;

pkg load image;

% Original
I = imread('cameraman.tiff');
I = mat2gray(getHead(I));
figure('Name', 'Original', 'NumberTitle', 'off');
imagesc(I); colormap('gray');

%% --------------------------Downscaling----------------------------- %%

%Scale down to 25% with Modulo 4(removing pixels)
%Not that smooth, higher differnece in neighbouring pixels
Down_MOD = downscale_MOD(I);

%Scale down to 25% with Averaging 4 pixels
%Looks a lot smoother than Modulo 4
Down_AVG = downscale_AVG(I);

%Scale down to 25% with imresize
%Looks the smoothest by very little in comparison with AVG
Down_imresize = imresize(I, 0.25);

%figure('Name', 'Original Small', 'NumberTitle', 'off');
%imagesc(Down_imresize); colormap('gray');

%% ----------------------------Upscaling----------------------------- %%

%Scale image up to original size(400%) with Nearest Neighbor Interpolation
    %as implemented in imresize
Up_Nearest_imresize = imresize(Down_imresize, 4, 'nearest');

%Scale image up to original size(400%) with Bilinear Interpolation first
    %Horizontically then Vertically 
Up_BiLin_HV = upscale_BiLin_HV(Down_imresize);

%Scale image up to original size(400%) with Bilinear Interpolation first
    %Vertically then Horizontically  
Up_BiLin_VH = upscale_BiLin_VH(Down_imresize);

%Scale image up to original size(400%) with Bilinear Interpolation as
    %implemented in imresize
Up_BiLin_imresize = imresize(Down_imresize, 4, 'bilinear');

%Scale image up to original size(400%) with Bicubic Interpolation first
    %Horizontically then Vertically 
Up_BiCub_HV = upscale_BiCub_HV(Down_imresize);

%Scale image up to original size(400%) with Bicubic Interpolation first
    %Vertically then Horizontically 
Up_BiCub_VH = upscale_BiCub_VH(Down_imresize);

M3 = [0 1 0; 1 -4 1; 0 1 0]; % Laplacian
fIM3 = imfilter(Up_BiCub_HV, M3);
%figure('Name', 'Masker 3(Tweede Afgeleide/Verskil (Laplacian Funksie)) - [0 1 0; 1 -4 1; 0 1 0]', 'NumberTitle', 'off');
%imagesc(fIM3);colormap('gray');

%Scale image up to original size(400%) with Catmull-Rom Bicubic Spline
    %Interpolation first Horizontically then Vertically
%A bit more aliasing than normal bicubic
Up_BiCub_HV_CatRom = upscale_BiCub_HV_CatRom(Down_imresize);

%Scale image up to original size(400%) with Hermite Bicubic Interpolation 
    %first Horizontically then Vertically
%Results in a very aliased image.
%Grid like pattern found when having low tension, a tension value of 1
    %provides desireable result.
%No noticable effect found on changing the bias value.
Up_BiCub_HV_Hermite = upscale_BiCub_HV_Hermite(Down_imresize);

%Scale image up to original size(400%) with Bicubic Interpolation 
    %as described in the textbook A Computational Introduction to Digitial 
    %Image Processing by McAndrew first Horizontically then Vertically
Up_BiCub_HV_McAndrew = upscale_BiCub_HV_McAndrew(Down_imresize);

%Scale image up to original size(400%) with Mitchell's and Netravalli's 
    %Bicubic Interpolation first Horizontically then Vertically
Up_BiCub_HV_MitchellNetravali = upscale_BiCub_HV_MitchellNetravali(Down_imresize);

%Scale image up to original size(400%) with B-Spline  Bicubic 
    %Interpolation first Horizontically then Vertically
Up_BiCub_HV_BSpline = upscale_BiCub_HV_BSpline(Down_imresize);

%Scale image up to original size(400%) with Mitchell's and Netravalli's 
    %Bicubic Interpolation first Horizontically then Vertically
Up_BiCub_HV_CSpline = upscale_BiCub_HV_CSpline(Down_imresize);

%Scale image up to original size(400%) with Bicubic Interpolation 
    %as implemented in imresize 
Up_BiCub_imresize = imresize(Down_imresize, 4, 'bicubic');

%% -------------------------Visual Camparisons------------------------- %%
%Comparing downscaling algorithms on images
%imresize looks smoothest by little compared to AVG
%MOD 4 looks worst
%display_downscaled(Down_MOD, Down_AVG, Down_imresize);

%Comparing upscaling algorithms on images
%display_upscaled(Up_BiLin_imresize, Up_BiLin_HV, Up_Nearest_imresize);

%Comparing bicubic interpolation
%Catmull-Rom has a bit more aliasing, otherwise looks almost the same as
    %normal cubic interpolation.
%Hermite interpolation results in a very aliased image.
display_upscaled_BiCub(Up_BiCub_HV, Up_BiCub_HV_CatRom, Up_BiCub_HV_Hermite, Up_BiCub_HV_McAndrew, Up_BiCub_HV_MitchellNetravali, Up_BiCub_HV_BSpline, Up_BiCub_HV_CSpline, Up_BiCub_imresize);

%Comparing upscaling algorithms ordering on images
%Difference in shade noticed, one is darker, other is lighter
%display_ordering(Up_BiLin_HV, Up_BiLin_VH, ' Bilinear Interpolation');
%display_ordering(Up_BiCub_HV, Up_BiCub_VH, ' Bicubic Interpolation');

%Up_Unsharp = nlfilter(Up_BiCub_HV, [3,3], 'max(x(:))');
%Up_Unsharp = nlfilter(Up_Unsharp, [3,3], 'min(x(:))');
%figure('Name', 'Additional Unsharp Masking', 'NumberTitle', 'off');   
%imagesc(Up_Unsharp); colormap('gray');

%% -------------------------RMSD & PSNR Comparisons--------------------- %%
disp('Original vs Nearest Neighbor');
disp(sprintf('RMSD = %f', RMSD(I, Up_Nearest_imresize)));
disp(sprintf('PSNR = %f', PSNR(I, Up_Nearest_imresize)));

disp('Original vs Bilinear imresize');
disp(sprintf('RMSD = %f', RMSD(I, Up_BiLin_imresize)));
disp(sprintf('PSNR = %f', PSNR(I, Up_BiLin_imresize)));

disp('Original vs Bilinear H->V');
disp(sprintf('RMSD = %f', RMSD(I, Up_BiLin_HV)));
disp(sprintf('PSNR = %f', PSNR(I, Up_BiLin_HV)));

disp('Original vs Bilinear V->H');
disp(sprintf('RMSD = %f', RMSD(I, Up_BiLin_VH)));
disp(sprintf('PSNR = %f', PSNR(I, Up_BiLin_VH)));

disp('Bilinear H->V vs Bilinear V->H');
disp(sprintf('RMSD = %f', RMSD(Up_BiLin_HV, Up_BiLin_VH)));
disp(sprintf('PSNR = %f', PSNR(Up_BiLin_HV, Up_BiLin_VH)));

disp('Original vs Bicubic H->V');
disp(sprintf('RMSD = %f', RMSD(I, Up_BiCub_HV)));
disp(sprintf('PSNR = %f', PSNR(I, Up_BiCub_HV)));

disp('Original vs Bicubic V->H');
disp(sprintf('RMSD = %f', RMSD(I, Up_BiCub_VH)));
disp(sprintf('PSNR = %f', PSNR(I, Up_BiCub_VH)));

disp('Bicubic H->V vs Bicubic V->H');
disp(sprintf('RMSD = %f', RMSD(Up_BiCub_HV, Up_BiCub_VH)));
disp(sprintf('PSNR = %f', PSNR(Up_BiCub_HV, Up_BiCub_VH)));

disp('Original vs Bicubic Hermite');
disp(sprintf('RMSD = %f', RMSD(I, Up_BiCub_HV_Hermite)));
disp(sprintf('PSNR = %f', PSNR(I, Up_BiCub_HV_Hermite)));

disp('Original vs Bicubic B-Spline');
disp(sprintf('RMSD = %f', RMSD(I, Up_BiCub_HV_BSpline)));
disp(sprintf('PSNR = %f', PSNR(I, Up_BiCub_HV_BSpline)));

disp('Original vs Bicubic C-Spline');
disp(sprintf('RMSD = %f', RMSD(I, Up_BiCub_HV_CSpline)));
disp(sprintf('PSNR = %f', PSNR(I, Up_BiCub_HV_CSpline)));

disp('Original vs Bicubic Mitchell-Netravali');
disp(sprintf('RMSD = %f', RMSD(I, Up_BiCub_HV_MitchellNetravali)));
disp(sprintf('PSNR = %f', PSNR(I, Up_BiCub_HV_MitchellNetravali)));

disp('Original vs Bicubic McAndrew');
disp(sprintf('RMSD = %f', RMSD(I, Up_BiCub_HV_McAndrew)));
disp(sprintf('PSNR = %f', PSNR(I, Up_BiCub_HV_McAndrew)));

disp('Original vs Bicubic imresize');
disp(sprintf('RMSD = %f', RMSD(I, Up_BiCub_imresize)));
disp(sprintf('PSNR = %f', PSNR(I, Up_BiCub_imresize)));