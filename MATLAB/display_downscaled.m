function display_downscaled(Down_MOD, Down_AVG, Down_imresize)
%Displays resulting downscaled images for comparison
    
    figure('Name', 'Scaled down to 25% with the Removal of Pixels, Modulo 4', 'NumberTitle', 'off');   
    imagesc(Down_MOD); colormap('gray');
    figure('Name', 'Scaled down to 25% with the Averaging of 4x4 Pixels', 'NumberTitle', 'off');
    imagesc(Down_AVG); colormap('gray');

    figure('Name', 'Scaled down to 25% by imresize()', 'NumberTitle', 'off');
    imagesc(Down_imresize); colormap('gray');
end