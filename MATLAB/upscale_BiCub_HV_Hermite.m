function Up = upscale_BiCub_HV_Hermite(I)
%Scale the image up to 200% with Hermite Bicubic Interpolation
%First Horizontically then Vertically
%40x40 -> 160x160
%Grid like pattern found when having low tension, a tension value of 1
    %provides desireable result.
%No noticable effect found on changing the bias value.
    
    Up = zeros(size(I, 1) .* 4, size(I, 2) .* 4);
    PreUp = zeros(size(I, 1), size(I, 2) .* 4);
    new_pixel_y = (size(I, 1) - 1) ./ (size(Up, 1) - 1);% 1 ./ 4
    new_pixel_x = (size(I, 2) - 1) ./ (size(Up, 2) - 1);% 1 ./ 4
    tension = 1;%1 is high, 0 is normal, -1 is low
    bias = 1;%0 is even, positive is towards first segment, negative is towards the other
    
    %1. Horizontical Interpolation
    for i = 1 : size(I, 1)
        PreUp(i, 1) = I(i, 1);
        for ii = 2 : size(PreUp, 2) - 1
            
            lambda = (ii - 1) .* new_pixel_x - floor((ii - 1) .* new_pixel_x);
            
            if(ii < 10)
                x1 = 1;
                x0 = 1;
                x2 = ceil((ii - 1) .* new_pixel_y);
                x3 = ceil((ii - 1) .* new_pixel_y) + 1;
            else
                x0 = floor((ii - 1) .* new_pixel_y) - 1;
                x1 = floor((ii - 1) .* new_pixel_y);
                x2 = ceil((ii - 1) .* new_pixel_y);
                x3 = ceil((ii - 1) .* new_pixel_y) + 1;
            end;
            
            y3 = I(i, x3);
            y2 = I(i, x2);
            y0 = I(i, x0);
            y1 = I(i, x1);
            
            m0 = ((y1 - y0) .* (1 + bias) .* (1 - tension) ./ 2) + ((y2 - 1) .* (1 - bias) .* (1 - tension) ./ 2);
            m1 = ((y2 - y1) .* (1 + bias) .* (1 - tension) ./ 2) + ((y3 - y2) .* (1 - bias) .* (1 - tension) ./ 2);
            
            a0 = 2 .* lambda.^3 - 3 .* lambda.^2 + 1;
            a1 = lambda.^3 - 2 .* lambda.^2 + lambda;
            a2 = lambda.^3 - lambda.^2;
            a3 = -2 .* lambda.^3 + 3 .* lambda.^2;
            
            PreUp(i, ii) = a0 .* y1 + a1 .* m0 + a2 .* m1 + a3 .* y2;
        end;
        PreUp(i, size(Up, 2)) = I(i, size(I, 2));
    end;
    %2. Vertical Interpolation
    for iii = 1 : size(Up, 2)
        Up(1, iii) = PreUp(1, iii);
        for iv = 2 : size(Up, 1) - 1
            
            lambda = (iv - 1) .* new_pixel_y - floor((iv - 1) .* new_pixel_y);
            
            if(iv < 10)
                x1 = 1;
                x0 = 1;
                x2 = ceil((iv - 1) .* new_pixel_y);
                x3 = ceil((iv - 1) .* new_pixel_y) + 1;
            else
                x0 = floor((iv - 1) .* new_pixel_y) - 1;
                x1 = floor((iv - 1) .* new_pixel_y);
                x2 = ceil((iv - 1) .* new_pixel_y);
                x3 = ceil((iv - 1) .* new_pixel_y) + 1;
            end;
            
            y0 = PreUp(x0, iii);
            y3 = PreUp(x3, iii);
            y2 = PreUp(x2, iii);
            y1 = PreUp(x1, iii);
            
            m0 = ((y1 - y0) .* (1 + bias) .* (1 - tension) ./ 2) + ((y2 - 1) .* (1 - bias) .* (1 - tension) ./ 2);
            m1 = ((y2 - y1) .* (1 + bias) .* (1 - tension) ./ 2) + ((y3 - y2) .* (1 - bias) .* (1 - tension) ./ 2);
            
            a0 = 2 .* lambda.^3 - 3 .* lambda.^2 + 1;
            a1 = lambda.^3 - 2 .* lambda.^2 + lambda;
            a2 = lambda.^3 - lambda.^2;
            a3 = -2 .* lambda.^3 + 3 .* lambda.^2;
            
            Up(iv, iii) = a0 .* y1 + a1 .* m0 + a2 .* m1 + a3 .* y2;
        end;
        Up(size(Up, 1), iii) = PreUp(size(PreUp, 1), iii);
    end;

end