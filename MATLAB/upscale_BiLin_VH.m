function Up = upscale_BiLin_VH(I)
%Scale the image up to 200% with Bilinear Interpolation
%First Vertically then Horizontically
%40x40 -> 160x160

    Up = zeros(size(I, 1) .* 4, size(I, 2) .* 4);
    PreUp = zeros(size(I, 1) .* 4, size(I, 2));
    new_pixel_y = (size(I, 1) - 1) ./ (size(Up, 1) - 1);% 1 ./ 4
    new_pixel_x = (size(I, 2) - 1) ./ (size(Up, 2) - 1);% 1 ./ 4
    
    %1. Vertical Interpolation
    for iii = 1 : size(I, 2)
        PreUp(1, iii) = I(1, iii);
        for iv = 2 : size(PreUp, 1) - 1
            lambda = (iv - 1) .* new_pixel_y - floor((iv - 1) .* new_pixel_y);
            y2 = I(ceil((iv - 1) .* new_pixel_y) + 1, iii);
            y1 = I(floor((iv - 1) .* new_pixel_y) + 1, iii);
            PreUp(iv, iii) = lambda .* y2 + (1 - lambda) .* y1;
        end;
        PreUp(size(Up, 1), iii) = I(size(I, 1), iii);
    end;
    
    %2. Horizontical Interpolation
    for i = 1 : size(PreUp, 1)
        Up(i, 1) = PreUp(i, 1);
        for ii = 2 : size(Up, 2) - 1
            lambda = (ii - 1) .* new_pixel_x - floor((ii - 1) .* new_pixel_x);
            y2 = PreUp(i, ceil((ii - 1) .* new_pixel_x) + 1);
            y1 = PreUp(i, floor((ii - 1) .* new_pixel_x) + 1);
            Up(i, ii) = lambda .* y2 + (1 - lambda) .* y1;
        end;
        Up(i, size(Up, 2)) = PreUp(i, size(I, 2));
    end;
   
end

