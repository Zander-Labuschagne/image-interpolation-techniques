function MSE = MSE(I1, I2)
%Compute Mean Square Error(MSE) for comparrison of images

    n = size(I1, 1);
    m = size(I1, 2);
    sum = 0;

    for i = 1 : n - 1
        for j = 1 : m - 1
            sum = sum + ((I1(i, j) - I2(i, j)).^2);
        end;
    end;

    MSE = sum ./ (n .* m);
            
end

