function R = R3(u)
%R3(u) function from A Computational Introduction to Digitial Image 
    %Processing by McAndrew
% This function is used for cubic interpolation
    if(abs(u) <= 1)
        R = 1.5 .* abs(u).^3 - 2.5 .* abs(u).^2 + 1;
    elseif(abs(u) > 1 && abs(u) <= 2)
        R = -0.5 .* abs(u).^3 + 2.5 .* abs(u).^2 - 4 .* abs(u) + 2;
    end;
 end

