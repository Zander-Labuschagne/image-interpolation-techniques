function Down = downscale_MOD(I)
%Scale the image down to 25% with the removal of pixels, MOD 4
%160x160 -> 40x40

    Down = zeros(size(I, 1) ./ 4, size(I, 2) ./ 4);
    
    iii = 1;
    iv = 1;
    for i = 1 : size(I, 1)
        if mod(i, 4) == 0
            for ii = 1 : size(I, 2)
                if mod(ii, 4) == 0
                    Down(iii, iv) = I(i, ii);
                    iv = iv + 1;
                end;
            end;
            iv = 1;
            iii = iii + 1;
        end;
    end;

end

