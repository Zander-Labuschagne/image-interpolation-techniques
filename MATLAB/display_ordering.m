function display_ordering(Up_HV, Up_VH, algo)
%Displays resulting upscaled images for comparison
    
    figure('Name', strcat('Scaled up to 400% with ', algo, ', H->V'), 'NumberTitle', 'off');   
    imagesc(Up_HV); colormap('gray');
    
    figure('Name', strcat('Scaled up to 400% with ', algo, ', V->H'), 'NumberTitle', 'off');   
    imagesc(Up_VH); colormap('gray');

end

