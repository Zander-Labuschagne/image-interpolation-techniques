function Up = upscale_BiCub_HV_CSpline(I)
%Scale the image up to 200% with Bicubic Interpolation as described in the 
    %textbook A Computational Introduction to Digitial Image Processing by 
    %McAndrew
%First Horizontically then Vertically
%40x40 -> 160x160
    
    Up = zeros(size(I, 1) .* 4, size(I, 2) .* 4);
    PreUp = zeros(size(I, 1), size(I, 2) .* 4);
    new_pixel_y = (size(I, 1) - 1) ./ (size(Up, 1) - 1);% 1 ./ 4
    new_pixel_x = (size(I, 2) - 1) ./ (size(Up, 2) - 1);% 1 ./ 4
    b = 0;
    c = 1;
    
    %1. Horizontical Interpolation
    for i = 1 : size(I, 1)
        PreUp(i, 1) = I(i, 1);
        for ii = 2 : size(PreUp, 2) - 1
            
            lambda = (ii - 1) .* new_pixel_x - floor((ii - 1) .* new_pixel_x);
            
            if(ii < 10)
                x1 = 1;
                x0 = 1;
                x2 = ceil((ii - 1) .* new_pixel_y);
                x3 = ceil((ii - 1) .* new_pixel_y) + 1;
            else
                x0 = floor((ii - 1) .* new_pixel_y) - 1;
                x1 = floor((ii - 1) .* new_pixel_y);
                x2 = ceil((ii - 1) .* new_pixel_y);
                x3 = ceil((ii - 1) .* new_pixel_y) + 1;
            end;
            
            y3 = I(i, x3);
            y2 = I(i, x2);
            y0 = I(i, x0);
            y1 = I(i, x1);
            
            PreUp(i, ii) = ((-1./6 .* b - c) .* y0 + (-3./2 .* b - c + 2) .* y1 + (3./2 .* b + c - 2) .* y2 + (1./6 .* b + c) .* y3) .* lambda.^3 + ((1./2 .* b + 2 .* c) .* y0 + (2 .* b + c - 3) .* y1 + (-5./2 .* b - 2 .* c + 3) .* y2 - c .* y3) .* lambda.^2 + ((-1./2 .* b - c) .* y0 + (1./2 .* b + c) .* y2) .* lambda + 1./6 .* b .* y0 + (-1./3 .* b + 1) .* y1 + 1./6 .* b .* y2;
        end;
        PreUp(i, size(Up, 2)) = I(i, size(I, 2));
    end;
    %2. Vertical Interpolation
    for iii = 1 : size(Up, 2)
        Up(1, iii) = PreUp(1, iii);
        for iv = 2 : size(Up, 1) - 1
            
            lambda = (iv - 1) .* new_pixel_y - floor((iv - 1) .* new_pixel_y);
            
            if(iv < 10)
                x1 = 1;
                x0 = 1;
                x2 = ceil((iv - 1) .* new_pixel_y);
                x3 = ceil((iv - 1) .* new_pixel_y) + 1;
            else
                x0 = floor((iv - 1) .* new_pixel_y) - 1;
                x1 = floor((iv - 1) .* new_pixel_y);
                x2 = ceil((iv - 1) .* new_pixel_y);
                x3 = ceil((iv - 1) .* new_pixel_y) + 1;
            end;
            
            y0 = PreUp(x0, iii);
            y3 = PreUp(x3, iii);
            y2 = PreUp(x2, iii);
            y1 = PreUp(x1, iii);
            
            Up(iv, iii) = ((-1./6 .* b - c) .* y0 + (-3./2 .* b - c + 2) .* y1 + (3./2 .* b + c - 2) .* y2 + (1./6 .* b + c) .* y3) .* lambda.^3 + ((1./2 .* b + 2 .* c) .* y0 + (2 .* b + c - 3) .* y1 + (-5./2 .* b - 2 .* c + 3) .* y2 - c .* y3) .* lambda.^2 + ((-1./2 .* b - c) .* y0 + (1./2 .* b + c) .* y2) .* lambda + 1./6 .* b .* y0 + (-1./3 .* b + 1) .* y1 + 1./6 .* b .* y2;
        end;
        Up(size(Up, 1), iii) = PreUp(size(PreUp, 1), iii);
    end;

end