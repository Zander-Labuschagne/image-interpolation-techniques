function Up = upscale_BiCub_VH(I)
%Scale the image up to 200% with Bicubic Interpolation
%First Vertically then Horizontically
%40x40 -> 160x160
    
    Up = zeros(size(I, 1) .* 4, size(I, 2) .* 4);
    PreUp = zeros(size(I, 1) .* 4, size(I, 2));
    new_pixel_y = (size(I, 1) - 1) ./ (size(Up, 1) - 1);% 1 ./ 4
    new_pixel_x = (size(I, 2) - 1) ./ (size(Up, 2) - 1);% 1 ./ 4
    
    %1. Vertical Interpolation
    for iii = 1 : size(I, 2)
        PreUp(1, iii) = I(1, iii);
        for iv = 2 : size(PreUp, 1) - 1
            
            lambda = (iv - 1) .* new_pixel_y - floor((iv - 1) .* new_pixel_y);
            
            if(iv < 10)
                x1 = 1;
                x0 = 1;
                x2 = ceil((iv - 1) .* new_pixel_y);
                x3 = ceil((iv - 1) .* new_pixel_y) + 1;
            else
                x0 = floor((iv - 1) .* new_pixel_y) - 1;
                x1 = floor((iv - 1) .* new_pixel_y);
                x2 = ceil((iv - 1) .* new_pixel_y);
                x3 = ceil((iv - 1) .* new_pixel_y) + 1;
            end;
            
            y0 = I(x0, iii);
            y3 = I(x3, iii);
            y2 = I(x2, iii);
            y1 = I(x1, iii);
            
            a0 = y3 - y2 - y0 + y1;
            a1 = y0 - y1 - a0;
            a2 = y2 - y0;
            a3 = y1;
            
            PreUp(iv, iii) = a0 .* lambda.^3 + a1 .* lambda.^2 + a2 .* lambda + a3;

        end;
        PreUp(size(PreUp, 1), iii) = I(size(I, 1), iii);
    end;
    
    %2. Horizontical Interpolation
    for i = 1 : size(PreUp, 1)
        Up(i, 1) = PreUp(i, 1);
        for ii = 2 : size(Up, 2) - 1
            
            lambda = (ii - 1) .* new_pixel_x - floor((ii - 1) .* new_pixel_x);
            
            if(ii < 10)
                x1 = 1;
                x0 = 1;
                x2 = ceil((ii - 1) .* new_pixel_y);
                x3 = ceil((ii - 1) .* new_pixel_y) + 1;
            else
                x0 = floor((ii - 1) .* new_pixel_y) - 1;
                x1 = floor((ii - 1) .* new_pixel_y);
                x2 = ceil((ii - 1) .* new_pixel_y);
                x3 = ceil((ii - 1) .* new_pixel_y) + 1;
            end;
            
            y3 = PreUp(i, x3);
            y2 = PreUp(i, x2);
            y0 = PreUp(i, x0);
            y1 = PreUp(i, x1);
            
            a0 = y3 - y2 - y0 + y1;
            a1 = y0 - y1 - a0;
            a2 = y2 - y0;
            a3 = y1;
                
            Up(i, ii) = a0 .* lambda.^3 + a1 .* lambda.^2 + a2 .* lambda + a3;

        end;
        Up(i, size(Up, 2)) = PreUp(i, size(PreUp, 2));
    end;
end