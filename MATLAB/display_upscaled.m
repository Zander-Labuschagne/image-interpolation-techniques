function display_upscaled(Up_BiLin, Up_BiCub, Up_NEDI)
%Displays resulting upscaled images for comparison
    
    figure('Name', 'Scaled up to 400% with Bilinear Interpolation, H->V', 'NumberTitle', 'off');   
    imagesc(Up_BiLin); colormap('gray');
    
    figure('Name', 'Scaled up to 400% with Bicubic Interpolation, H->V', 'NumberTitle', 'off');   
    imagesc(Up_BiCub); colormap('gray');
    
    figure('Name', 'Scaled up to 200% with New Edge Directed Interpolation', 'NumberTitle', 'off');   
    imagesc(Up_NEDI); colormap('gray');

end

