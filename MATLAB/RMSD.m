function RMSD = RMSD(I1, I2)
%Compute Root Mean Square Deviation(RMSD) for comparrison of images

    RMSD = sqrt(MSE(I1, I2));
            
end

