function Up = upscale_BiLin_HV(I)
%Scale the image up to 200% with Bilinear Interpolation
%First Horizontically then Vertically
%40x40 -> 160x160

    Up = zeros(size(I, 1) .* 4, size(I, 2) .* 4);
    PreUp = zeros(size(I, 1), size(I, 2) .* 4);
    new_pixel_y = (size(I, 1) - 1) ./ (size(Up, 1) - 1);% 1 ./ 4
    new_pixel_x = (size(I, 2) - 1) ./ (size(Up, 2) - 1);% 1 ./ 4
    
    %1. Horizontical Interpolation
    for i = 1 : size(I, 1)
        PreUp(i, 1) = I(i, 1);
        for ii = 2 : size(PreUp, 2) - 1
            lambda = (ii - 1) .* new_pixel_x - floor((ii - 1) .* new_pixel_x);
            y2 = I(i, ceil((ii - 1) .* new_pixel_x) + 1);
            y1 = I(i, floor((ii - 1) .* new_pixel_x) + 1);
            PreUp(i, ii) = lambda .* y2 + (1 - lambda) .* y1;
        end;
        PreUp(i, size(Up, 2)) = I(i, size(I, 2));
    end;
    %2. Vertical Interpolation
    for iii = 1 : size(Up, 2)
        Up(1, iii) = PreUp(1, iii);
        for iv = 2 : size(Up, 1) - 1
            lambda = (iv - 1) .* new_pixel_y - floor((iv - 1) .* new_pixel_y);
            y2 = PreUp(ceil((iv - 1) .* new_pixel_y) + 1, iii);
            y1 = PreUp(floor((iv - 1) .* new_pixel_y) + 1, iii);
            Up(iv, iii) = lambda .* y2 + (1 - lambda) .* y1;
        end;
        Up(size(Up, 1), iii) = PreUp(size(PreUp, 1), iii);
    end;
end

