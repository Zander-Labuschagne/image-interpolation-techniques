function H = getHead(Cameraman)
%Return a picture of the camera man's head 160x160
    
    H = zeros(160, 160);
    for i = 70 : 229
        for ii = 180 : 339
            H(i - 69, ii - 179) = Cameraman(i, ii);
        end;
    end;
end

