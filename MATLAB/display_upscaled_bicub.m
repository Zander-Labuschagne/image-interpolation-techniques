function display_upscaled_bicub(Up_BiCub, Up_BiCub_CatmullRom, Up_BiCub_Hermite, Up_BiCub_McAndrew, Up_BiCub_MitchellNetravali, Up_BiCub_BSpline, Up_BiCub_CSpline, Up_BiCub_imresize)
%Displays resulting upscaled images for comparison
    
    figure('Name', 'Scaled up to 400% with Bicubic Interpolation, H->V', 'NumberTitle', 'off');   
    imagesc(Up_BiCub); colormap('gray');
    
    figure('Name', 'Scaled up to 400% with Catmull-Rom Bicubic Spline Interpolation, H->V', 'NumberTitle', 'off');   
    imagesc(Up_BiCub_CatmullRom); colormap('gray');
    
    figure('Name', 'Scaled up to 400% with Hermite Bicubic Interpolation, H->V', 'NumberTitle', 'off');   
    imagesc(Up_BiCub_Hermite); colormap('gray');

    figure('Name', 'Scaled up to 400% with Bicubic Interpolation as Described by McAndrew, H->V', 'NumberTitle', 'off');   
    imagesc(Up_BiCub_McAndrew); colormap('gray');
    
    figure('Name', 'Scaled up to 400% with Mitchell and Netravali Bicubic Interpolation, H->V', 'NumberTitle', 'off');   
    imagesc(Up_BiCub_MitchellNetravali); colormap('gray');
    
    figure('Name', 'Scaled up to 400% with B-Spline Bicubic Interpolation, H->V', 'NumberTitle', 'off');   
    imagesc(Up_BiCub_BSpline); colormap('gray');
    
    figure('Name', 'Scaled up to 400% with C-Spline Bicubic Interpolation, H->V', 'NumberTitle', 'off');   
    imagesc(Up_BiCub_CSpline); colormap('gray');
    
    figure('Name', 'Scaled up to 400% with Bicubic Interpolation with imresize', 'NumberTitle', 'off');   
    imagesc(Up_BiCub_imresize); colormap('gray');

end

