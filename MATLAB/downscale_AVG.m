function Down = downscale_AVG(I)
%Downsize the image by 25% with the averaging of 4x4 pixels
%160x160 -> 40x40

    Down = zeros(size(I, 1) ./ 4, size(I, 2) ./ 4);
    
    ny = 1;
    for iii = 1 : size(Down, 1)
        nx = 1;
        for iv  = 1 : size(Down, 2)
            sum = 0;
            for i = ny : ny + 3
                for ii = nx : nx + 3
                    sum = sum + I(i, ii);
                end;
            end;
            nx = ii + 1;
            avg = sum ./ 16;
            Down(iii, iv) = avg;
        end;
        ny = i + 1;
    end;
    
end


